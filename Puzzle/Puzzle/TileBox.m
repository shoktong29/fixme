//
//  TileBox.m
//  FixMe
//
//  Created by Martin on 12/19/12.
//  Copyright (c) 2012 ripplewave. All rights reserved.
//

#import "TileBox.h"

@implementation TileBox

- (id)initWithData:(NSDictionary *)data;
{
    self = [super init];
    if (self) {
        _tileSize = CGSizeFromString([data objectForKey:@"tileSize"]);
        _debug = [[UIView alloc]init];
        _debug.frame = (CGRect){0,0,.size=_tileSize};
        _debug.backgroundColor = [UIColor blackColor];
        // prevents from showing the debug outside the parentView
//        [self addSubview:_debug];
    }
    return self;
}


- (void)setDefault{
    self.backgroundColor = [UIColor purpleColor];
    self.clipsToBounds = YES; 
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
//    NSLog(@"%@",NSStringFromCGPoint(point));
    //get the coord of tile selected
    int a = point.x / 60;
    int b = point.y / 60;
    NSLog(@"Row:%d | Col:%d",b,a);
    
    //get the actual rect of selected coord
    if (!_debug) {
        _debug = [[UIView alloc]init];
        _debug.frame = (CGRect){0,0,.size=_tileSize};
        _debug.backgroundColor = [UIColor blackColor];
        [self addSubview:_debug];
    }

    CGPoint tileCenter;
    tileCenter.x = (a *_tileSize.width)+_tileSize.width/2;
    tileCenter.y = (b *_tileSize.height)+_tileSize.height/2;
    
    _debug.center = tileCenter;
    return self;
}

@end
