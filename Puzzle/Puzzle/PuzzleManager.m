//
//  PuzzleManager.m
//  Puzzle
//
//  Created by Martin on 11/6/13.
//  Copyright (c) 2013 Martin Magalong. All rights reserved.
//

#import "PuzzleManager.h"
#import "UIImage+Helper.h"

//#define kROWS 5
//#define kCOLS 5
//#define kTILESIZE 64

@implementation PuzzleManager{
    UIView *debug;
    int kROWS;
    int kCOLS;
    int kTILESIZE;
    Tile *tileMissing;
}


#pragma mark - Lifecycle
- (id)init
{
    self = [super init];
    if (self) {
        kROWS = 3;
        kCOLS = 3;
        kTILESIZE = 80;//ceilf(320/kROWS);
        self.frame = (CGRect){.origin=CGPointZero,.size={kCOLS*kTILESIZE,kROWS*kTILESIZE}};
        self.backgroundColor = [UIColor blackColor];
        [self setUserInteractionEnabled:YES];
        [self createTilesFromImage:nil];
        
    }
    return self;
}

- (void)cleanup{
    for (int x=0; x<_listTiles.count; x++) {
        [_listTiles[x] removeFromSuperview];
    }
    [tileMissing removeFromSuperview];
    tileMissing = nil;
    _listTiles = nil;
    _listTiles = nil;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[touches allObjects]objectAtIndex:0];
    CGPoint currentPoint = [touch locationInView:self];
    
//    if (!debug) {
//        debug = [[UIView alloc]init];
//        debug.frame = (CGRect){0,0,.size=CGSizeMake(kTILESIZE, kTILESIZE)};
//        debug.backgroundColor = [UIColor blackColor];
//        [self addSubview:debug];
//    }
    
    int col = currentPoint.x / kTILESIZE;
    int row = currentPoint.y / kTILESIZE;
    
    //get the actual rect of selected coord
    CGPoint tileCenter;
    tileCenter.x = (col *kTILESIZE)+kTILESIZE/2;
    tileCenter.y = (row *kTILESIZE)+kTILESIZE/2;
    
    debug.center = tileCenter;
    
    Tile *tile = [self getTileInPoint:CGPointMake(row, col)];
    Cell *cell = [self findValidMoveForTile:tile];
    if (cell != nil) {
        [self moveTile:tile toCell:cell];
    }
    NSLog(@"ISDOLVED: %d",[self isSolved]);
}

#pragma mark - Cell & Tile Helper
- (Tile *)getTileInPoint:(CGPoint)point{
    for (int x=0; x<_listTiles.count; x++) {
        Tile *tile = _listTiles[x];
        if (tile.mCoords.x == point.x && tile.mCoords.y == point.y) {
            return tile;
        }
    }
    return nil;
}

- (Cell *)getCellInPoint:(CGPoint)point{
    for (int x=0; x<_listCells.count; x++) {
        Cell *cell = _listCells[x];
        if (cell.coords.x == point.x && cell.coords.y == point.y) {
            return cell;
        }
    }
    return nil;
}

- (Cell *)findValidMoveForTile:(Tile *)tile{
    int row = tile.mCoords.x;
    int col = tile.mCoords.y;
    Cell *origin = [self getCellInPoint:CGPointMake(row, col)];

    NSArray *listNeighbors = [self getNeigborCellsOfTile:origin];
    for (int x=0; x<listNeighbors.count; x++) {
        Cell *cell = listNeighbors[x];
            if (cell.uniqueId >0 && !cell.isOccupied) {
                return cell;
            }
    }
    return nil;
}

- (NSArray *)getNeigborCellsOfTile:(Cell *)c{
    int row = c.coords.x;
    int col = c.coords.y;
    Cell *cell = [self getCellInPoint:CGPointMake(row+1, col)];
    Cell *cell2 = [self getCellInPoint:CGPointMake(row-1, col)];
    Cell *cell3 = [self getCellInPoint:CGPointMake(row, col+1)];
    Cell *cell4 = [self getCellInPoint:CGPointMake(row, col-1)];
    
    NSMutableArray *tList = [[NSMutableArray alloc]init];
    if (cell != nil)
        [tList addObject:cell];
    if (cell2 != nil)
        [tList addObject:cell2];
    if (cell3 != nil)
        [tList addObject:cell3];
    if (cell4 != nil)
        [tList addObject:cell4];
    
    return tList;
}

- (void)moveTile:(Tile *)tile toCell:(Cell *)cell{
    int row = tile.mCoords.x;
    int col = tile.mCoords.y;
    Cell *origin = [self getCellInPoint:CGPointMake(row, col)];
    
    tile.mCoords = cell.coords;
    tile.frame = cell.frame;
    cell.isOccupied = YES;
    origin.isOccupied = NO;

}

#pragma mark - Logic
- (void)createTilesFromImage:(UIImage *)image{
    [self cleanup];
    if (image) {
//        image = [UIImage scaleImage:image ToSize:CGSizeMake(kROWS*kTILESIZE,kROWS*kTILESIZE)];
        NSArray *temp = [UIImage generateTileFromImage:image size:CGSizeMake(kTILESIZE, kTILESIZE) prefix:@"JS"];
        NSMutableArray *listTileImages = [[NSMutableArray alloc]initWithArray:temp];
        NSMutableArray *tListTiles = [[NSMutableArray alloc]init];
        NSMutableArray *tListCells = [[NSMutableArray alloc]init];
        
        //Get location of images
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *directoryPath = [paths objectAtIndex:0];
        
        //create tile object and render it to view
        int counter = 0;
        for(int x=0; x<kROWS; x++){
            for(int y=0; y<kCOLS; y++){
                counter++;
                //create Cells for coords reference
                Cell *cell = [[Cell alloc]init];
                cell.uniqueId = counter;
                cell.coords = CGPointMake(x, y);
                cell.isOccupied = NO;
                cell.frame = CGRectMake(y*kTILESIZE, x*kTILESIZE, kTILESIZE, kTILESIZE);
                
                //create Tiles

                    NSString *filename = [[listTileImages objectAtIndex:x] objectAtIndex:y];
                    //NSDictionary *data = [[NSDictionary alloc]initWithObjectsAndKeys: filename,@"filename",NSStringFromCGRect(CGRectZero),@"frame", nil];
                    Tile *tile = [[Tile alloc]init];
                    tile.mTileId = counter;
                    tile.mCoords = CGPointMake(x, y);
                    tile.frame = CGRectMake(y*kTILESIZE, x*kTILESIZE, kTILESIZE, kTILESIZE);
                    tile.mName = [NSString stringWithFormat:@"%@/%@",directoryPath,filename];

                if (!(x == kCOLS-1 && y == kROWS-1)){ //dont add last piece
                    cell.isOccupied = YES;
                    [tListTiles addObject:tile];
                    [self addSubview:tile];
                }
                else{ //get reference of last piece
                    tileMissing = tile;
                }
                [tListCells addObject:cell];
            }
        }
        _listTiles = [[NSMutableArray alloc]initWithArray:tListTiles];
        _listCells = [[NSArray alloc] initWithArray:tListCells];
        
        //clean
        paths = nil;
        [tListCells removeAllObjects];
        tListCells = nil;
        [tListTiles removeAllObjects];
        tListTiles = nil;
        [listTileImages removeAllObjects];
        listTileImages = nil;
        temp = nil;
        
        [self shuffleWithMoves:100];
    }
}

- (void)shuffleWithMoves:(int)moves{
    Cell *cellBlank = nil;
    Cell *cellPrevious = nil;
    
    //find cell with the missing tile
    for (int x=0; x<_listCells.count; x++) {
        cellBlank = _listCells[x];
        Tile *tile = [self getTileInPoint:cellBlank.coords];
        if (tile == nil) {
            break;
        }
    }
    
//    Cell *candidateCell = nil;
    for (int x=0; x<=moves; x++) {
        //get neighbors of that cell
        NSArray *tListNeighbors = [self getNeigborCellsOfTile:cellBlank];
        int i = arc4random() % tListNeighbors.count;
        Cell *candidateCell = tListNeighbors[i];
        if (candidateCell != cellBlank && candidateCell != cellPrevious) {
            //get the tile on that candidateCell and move it to cell
            Tile *tile = [self getTileInPoint:candidateCell.coords];
            [self moveTile:tile toCell:cellBlank];
            cellPrevious = cellBlank;
            cellBlank = candidateCell; //replace the reference of the cellBlank to candidateCell
        }
    }
}

- (BOOL)isSolved{
    //loop through the cells and check if its id matches with the tile in the same coords
    for(int x=0; x<kROWS; x++){
        for(int y=0; y<kCOLS; y++){
            Tile *temp = [self getTileInPoint:CGPointMake(kROWS-1, kCOLS-1)];
            //stop process if tile exists in last cell
            if (temp != nil){
                return NO;
            }
            else{
                if (!(x == kCOLS-1 && y == kROWS-1)){ //
                    Cell *cell = [self getCellInPoint:CGPointMake(x, y)]; //get the cell in coords
                    Tile *tile = [self getTileInPoint:CGPointMake(x, y)];   //get tile in the coords
                    //compare the id of cell and coords and if not match stop process
                    if (tile == nil || (cell.uniqueId != tile.mTileId)) {
                        return NO;
                    }
                }
            }
        }
    }
    
    //cells' and tiles' ids matched with each other so puzzle is solved
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"CONGRATULATIONS!!!" message:@"no price for you though >:P" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dimsmiss", nil];
    [alert show];
    //add the missing piece
    [self addSubview:tileMissing];
    return YES;
}



@end
