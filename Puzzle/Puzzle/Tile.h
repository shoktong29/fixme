//
//  Tile.h
//  FixMe
//
//  Created by Martin on 12/19/12.
//  Copyright (c) 2012 ripplewave. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tile : UIImageView <UIGestureRecognizerDelegate>
@property (nonatomic, strong) NSString *mName;
@property (nonatomic, assign) int mTileId;
@property (nonatomic, assign) CGPoint mCoords;
//- (id)initWithData:(NSDictionary *)data;
@end
