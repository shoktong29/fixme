//
//  Cell.h
//  Puzzle
//
//  Created by Martin on 11/7/13.
//  Copyright (c) 2013 Martin Magalong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Cell : NSObject

@property (nonatomic) int uniqueId;
@property (nonatomic) CGPoint coords;
@property (nonatomic) CGRect frame;
@property (nonatomic) BOOL isOccupied;

@end
