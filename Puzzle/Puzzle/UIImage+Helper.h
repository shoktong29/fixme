//
//  UIImage+Helper.h
//  FixMe
//
//  Created by Martin on 12/19/12.
//  Copyright (c) 2012 ripplewave. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Helper)
/** Class method to generate tile images with tile size and filename prefix. */
+ (NSArray *)generateTileFromImage:(UIImage *)image size:(CGSize)size prefix:(NSString *)prefix;
+ (UIImage *)scaleImage:(UIImage *)image ToSize:(CGSize)newSize;
@end
