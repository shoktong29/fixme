//
//  PuzzleVC.m
//  Puzzle
//
//  Created by Martin on 10/14/13.
//  Copyright (c) 2013 Martin Magalong. All rights reserved.
//

#import "PuzzleVC.h"
#import "PuzzleManager.h"

@implementation PuzzleVC{
    PuzzleManager *manager;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    manager = [[PuzzleManager alloc]init];
    [self.view addSubview:manager];

}

- (IBAction)reset:(id)sender{
//    [manager removeFromSuperview];
//    manager = nil;
//    
    int x = arc4random() % 3;
    NSString *filename;
    switch (x) {
        case 0:
            filename = @"Screen Shot 2013-05-01 at 9.41.51 AM.png";
            break;
        case 1:
            filename = @"Screen Shot 2013-05-01 at 9.38.18 AM.png";
            break;
        case 2:
            filename = @"Screen Shot 2013-05-01 at 9.44.49 AM.png";
            break;
        default:
            break;
    }
    [manager createTilesFromImage:[UIImage imageNamed:filename]];
}

@end
