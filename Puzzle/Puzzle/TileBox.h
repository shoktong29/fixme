//
//  TileBox.h
//  FixMe
//
//  Created by Martin on 12/19/12.
//  Copyright (c) 2012 ripplewave. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TileBox : UIView{
    CGSize _tileSize;
    UIView *_debug;
}
- (id)initWithData:(NSDictionary *)data;
@end
