//
//  Tile.m
//  FixMe
//
//  Created by Martin on 12/19/12.
//  Copyright (c) 2012 ripplewave. All rights reserved.
//

#import "Tile.h"

@implementation Tile

- (id)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor blackColor];
        self.userInteractionEnabled = YES;
        
//        UILongPressGestureRecognizer *gesture = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(gestureTapped:)];
//        gesture.minimumPressDuration = 0.05f;
//        [self addGestureRecognizer:gesture];
    }
    return self;
}

- (void)setMName:(NSString *)mName{
    _mName = mName;
    self.image = [UIImage imageWithContentsOfFile:_mName];
}

#pragma mark - UIGesture Delegates

#pragma mark - Gesture
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    return YES;
}

- (void)gestureTapped:(UIGestureRecognizer *)gesture{
    switch (gesture.state) {
        case 1:
            NSLog(@"Tap started");
            break;
        case 2:
            NSLog(@"Dragging");
            gesture.view.center = [gesture locationInView: self.superview];
            break;
        case 3:
            NSLog(@"Tap ended");
            [self animate];
            break;
        default:
            break;
    }
}

- (void)animate{
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^(void) {
        CGAffineTransform tranform = CGAffineTransformMakeScale(1, -1);
        self.transform = tranform;
        self.backgroundColor = [UIColor whiteColor];
    }completion:^(BOOL finished) {
        CGAffineTransform tranform = CGAffineTransformMakeScale(1, 1);
        self.transform = tranform;
    }];
}

@end
