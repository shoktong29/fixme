//
//  main.m
//  Puzzle
//
//  Created by Martin on 10/14/13.
//  Copyright (c) 2013 Martin Magalong. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
