//
//  UIImage+Helper.m
//  FixMe
//
//  Created by Martin on 12/19/12.
//  Copyright (c) 2012 ripplewave. All rights reserved.
//

#import "UIImage+Helper.h"

@implementation UIImage (Helper)


+ (NSArray *)generateTileFromImage:(UIImage *)image size:(CGSize)size prefix:(NSString *)prefix{
    NSMutableArray *_tileImages = [[NSMutableArray alloc]init];
    int _cols = floorf(image.size.width / size.width);
    int _rows = floorf(image.size.height / size.height);
    
    for(int x=0; x<_rows; x++){
        NSMutableArray *temp = [[NSMutableArray alloc]init];
        [_tileImages addObject:temp];
        for(int y=0; y<_cols; y++)
        {
            NSString *filename = [NSString stringWithFormat:@"%@%d-%d.png",prefix,x,y];
            CGImageRef tileImage = CGImageCreateWithImageInRect(image.CGImage,
                                                                (CGRect){{y*size.width,x*size.height},size});
            NSData *imageData = UIImagePNGRepresentation([UIImage imageWithCGImage:tileImage]);
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            NSString *directoryPath = [[paths objectAtIndex:0]stringByAppendingString:[NSString stringWithFormat:@"/%@",filename]];
            [imageData writeToFile:directoryPath atomically:NO];
            [temp addObject:filename];
            
            directoryPath = nil;
            paths = nil;
            imageData = nil;
            filename = nil;
            tileImage = nil;
            filename = nil;
        }
    }
    return (NSArray *)_tileImages;
}

+ (UIImage *)scaleImage:(UIImage *)image ToSize:(CGSize)newSize {
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        if ([[UIScreen mainScreen] scale] == 2.0) {
            UIGraphicsBeginImageContextWithOptions(newSize, YES, 2.0);
        } else {
            UIGraphicsBeginImageContext(newSize);
        }
    } else {
        UIGraphicsBeginImageContext(newSize);
    }
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
@end
