//
//  PuzzleManager.h
//  Puzzle
//
//  Created by Martin on 11/6/13.
//  Copyright (c) 2013 Martin Magalong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Tile.h"
#import "Cell.h"

@interface PuzzleManager : UIView
@property (nonatomic, readonly) CGSize size;
@property (nonatomic, retain) NSArray *listCells;
@property (nonatomic, retain) NSArray *listTiles;

//- (id)initWithPuzzleSize:(CGSize )size;
- (void)createTilesFromImage:(UIImage *)image;
@end
